'use strict';

global.$ = {
    /**----- Основной таск -----**/
    gulp: require('gulp'),
    sass: require('gulp-sass'),
	autoprefixer: require('gulp-autoprefixer'),
    nunjucks: require('gulp-nunjucks-render'),
    useref: require('gulp-useref'),
    gulpif: require('gulp-if'),
    babel: require('gulp-babel'),
    browserSync: require('browser-sync'),
    notify: require('gulp-notify'),
    /**----- Сборка проекта -----**/
	// del: require('del'),
    // uglify: require('gulp-uglifyjs'),
	// tinypng: require('gulp-tinypng-compress'),
	// htmlmin: require('gulp-htmlmin'),
	// gcmq: require('gulp-group-css-media-queries'),
	// cleancss: require('gulp-clean-css'),
	// penthouse: require('penthouse'),
    // inject: require('gulp-inject-string'),
    /**----------- SEO -----------**/
    // sitemap: require('gulp-sitemap'),
    // robots: require('gulp-robots'),
    /**---------- Прочее ----------**/
    // rsync: require('gulp-rsync'),

    path: {
        config: require('./gulp_modules/config.js'),
        settings: require('./gulp_modules/settings.js')
    }
};

$.path.config.forEach(function (taskPath) {
    require(taskPath)();
});


/**----- Основные таски -----**/

$.gulp.task('default', $.gulp.series(
    $.gulp.parallel(htmlPreproc, cssPreproc),
    'useref',
    $.gulp.parallel('watch', 'browser-sync')
));

// $.gulp.task('build', $.gulp.series(
//     $.gulp.parallel('clean'),
//     $.gulp.parallel('build-css', 'build-font', 'build-js', 'build-img', 'build-html'),
//     $.gulp.parallel('penthouse')
// ));

// $.gulp.task('seo', $.gulp.series('robots', 'sitemap'));