module.exports = function(){
    $.gulp.task('rsync', function() {
        return $.gulp.src('dist/**')
        .pipe($.rsync({
            root: r.rsync.root,
            hostname: r.rsync.hostname,
            destination: r.rsync.destination,
            // include: ['*.htaccess'], // Includes files to deploy
            exclude: r.rsync.exclude, // Excludes files from deploy
            recursive: true,
            archive: true,
            silent: false,
            compress: true
        }))
    });
}