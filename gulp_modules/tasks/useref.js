module.exports = function(){
    $.gulp.task('useref', function(){
        return $.gulp.src(p.main.userefHtml) 
            .pipe($.useref()).on('error', $.notify.onError(function (error) {
                return "Useref error occurred: " + error.message;
            }))
            .pipe($.gulpif('_*.js', $.babel({
                presets: ['@babel/env']
            })))
            .pipe($.gulp.dest(p.main.html))
            .pipe($.browserSync.reload({stream: true}))
    });
}