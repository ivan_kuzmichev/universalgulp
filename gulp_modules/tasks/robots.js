module.exports = function(){
    $.gulp.task('robots', function(end) {
        $.gulp.src(d.robots.robots)
            .pipe($.robots({
                useragent: 'Googlebot, Yandex ',
                allow: [d.robots.allow],
                disallow: [d.robots.disallow]
            }))
            .pipe($.gulp.dest(d.robots.robotsBuild));
            end();
    });
}
