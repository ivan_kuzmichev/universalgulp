module.exports = function(){
	$.gulp.task('penthouse', function (end) {
		$.penthouse({
			url: d.penthouse.url,
			css: d.penthouse.css,
			width: 1280,
			height: 800
		}, function (err, criticalCss) {
		$.gulp.src(d.penthouse.url)
			.pipe($.inject.after('\n<style>\n' + criticalCss + '\n</style>'))
			.pipe($.gulp.dest('dist'))
		});
		end();
	});
}