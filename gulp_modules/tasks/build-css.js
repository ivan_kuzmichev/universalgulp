module.exports = function(){
    $.gulp.task('build-css', function(){
        return $.gulp.src(p.build.css)
            .pipe($.autoprefixer(['last 15 versions']))
            .pipe($.gcmq())
            .pipe($.cleancss({
                level: { 2: { specialComments: 0 },
                compatibility: 'ie9'
            }}))
            .pipe($.gulp.dest(p.build.cssBuild))
    });
}