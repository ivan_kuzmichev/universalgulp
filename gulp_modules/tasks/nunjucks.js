module.exports = function(){
    $.gulp.task('nunjucks', function(){
        return $.gulp.src([p.main.preprocHtml, p.main.preprocHtmlExclude]) 
            .pipe($.nunjucks({
                path: [p.main.preprocHtmlPath]
            })).on('error', $.notify.onError(function (error) {
                return "A task nunjucks error occurred: " + error.message;
            }))
            .pipe($.gulp.dest(p.main.html))
    });
}