global.baseDir = 'app';
global.buildDir = 'dist';
global.cssPreproc = 'scss';
global.htmlPreproc = 'nunjucks';
global.tinypngKey = 'khxIPqiJ0lt8o3IIY3Y5jdV2mo38Kiu6',
global.p = {
    main: {
        /** Css **/
        preprocCss: baseDir + '/' + cssPreproc + '/**/*.' + cssPreproc,
        css: baseDir + '/css',
        fullPathCss: baseDir + '/**/*.css',
        /** Html **/
        preprocHtml: baseDir + '/' + htmlPreproc + '/**/*.html',
        preprocHtmlExclude: '!' + baseDir + '/' + htmlPreproc + '/**/_*.html',
        preprocHtmlPath: baseDir + '/' + htmlPreproc + '/',
        userefHtml: baseDir + '/*.html',
        html: baseDir + '/',
        /** Js **/
        js: baseDir + '/js/**/*.js',
    },
    build: {
        /** Css **/
        css: baseDir + '/css/**/*',
        cssBuild: buildDir + '/css',
        /** Font **/
        font: baseDir + '/font/**/*',
        fontBuild: buildDir + '/font',
        /** Html **/
        html: baseDir + '/*.html',
        htmlBuild: buildDir,
        /** Img **/
        img: baseDir + '/img/**/*',
        imgBuild: buildDir + '/img',
        sigFile: buildDir + '/.tinypng-sigs',
        /** js **/
        js: baseDir + '/js/**/*',
        jsBuild: buildDir + '/js',
    },
},
global.r = {
    rsync: {
        root: buildDir + '/',
        hostname: 'username@yousite.com',
        destination: 'yousite/public_html/',
        exclude: '/node_modules'
    }
},
global.d = {
    penthouse: {
        url: buildDir + '/index.html',
		css: p.build.cssBuild + '/main.css'
    },
    robots: {
        robots: p.build.htmlBuild + '/index.html',
        robotsBuild: p.build.htmlBuild + '/',
        allow: buildDir + '/',
        disallow: baseDir + '/'
    },
    sitemap: {
        sitemap: buildDir + '*.html',
        sitemapBuild: buildDir,
        siteUrl: 'http://site'
    }
}